# partyscreen-backend

## Usage

View all flags and command with `./partyscreen-backend help`.

### With Binary

```bash
./partyscreen-backend -d ../partyscreen-frontend
```

### With Docker

View help:

```bash
docker run \
  --rm \
  registry.gitlab.com/homescreen.rocks/partyscreen-backend/master \
  --help
```

Run server:

```bash
docker run \
  --rm \
  -t \
  -i \
  -v $(pwd)/../partyscreen-frontent:/ui \
  -v partyscreen-data:/data \
  -p 9999:9999 \
  registry.gitlab.com/homescreen.rocks/partyscreen-backend/master \
  --ui-directory /ui \
  --data-directory /data
```

## Build

### Build Locally

Requirements:

* GNU Make
* git
* [Go](https://golang.org/) (tested with 1.10)
* [dep](https://github.com/golang/dep)
* This repo must be checked out to `$GOPATH/src/gitlab.com/homescreen.rocks/partyscreen-backend`.

Compile binary into the current working directory:

```bash
make build
```

Alternatively, test it and install it in `$GOPATH/bin`:

```bash
make install
```

### Build with Docker

```bash
docker build .
```
