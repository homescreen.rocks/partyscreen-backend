# Source: https://github.com/rebuy-de/golang-template
# Version: 2.0.3-snapshot

FROM golang:1.10-alpine as builder

RUN apk add --no-cache git make

# Configure Go
ENV GOPATH /go
ENV PATH /go/bin:$PATH
RUN mkdir -p ${GOPATH}/src ${GOPATH}/bin

# Install Go Tools
RUN go get -u github.com/golang/lint/golint
RUN go get -u github.com/golang/dep/cmd/dep

COPY . /go/src/gitlab.com/homescreen.rocks/partyscreen-backend
WORKDIR /go/src/gitlab.com/homescreen.rocks/partyscreen-backend
RUN CGO_ENABLED=0 make install


FROM alpine:latest
RUN apk add ca-certificates --no-cache
COPY --from=builder /go/bin/partyscreen-backend /usr/local/bin/
ENTRYPOINT ["/usr/local/bin/partyscreen-backend"]
