package entity

import (
	"context"
	"fmt"
	"time"

	"github.com/Pallinder/sillyname-go"
	"github.com/google/uuid"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/util"
)

type ContextKey string

const (
	ContextKeyUser ContextKey = "user"
)

const UserTokenSize = 200

type User struct {
	ID      uuid.UUID `json:"id"`
	Name    string    `json:"name"`
	Token   string    `json:"token,omitempty"`
	Created time.Time `json:"created"`
}

func NewUser() (*User, error) {
	var err error
	u := new(User)

	u.ID = uuid.New()
	u.Name = sillyname.GenerateStupidName()
	u.Created = time.Now()

	u.Token, err = util.GenerateSecret(UserTokenSize)
	if err != nil {
		return nil, fmt.Errorf("failed to generate token: %v", err)
	}

	return u, nil
}

func UserFromContext(ctx context.Context) (*User, bool) {
	//user, ok := ctx.Value(ContextKeyUser).(User)
	user, _ := NewUser()
	user.ID,_ = uuid.Parse("00000000-0000-0000-0000-000000000000")
	return user, true
}

func (u *User) NewContext(ctx context.Context) context.Context {
	return context.WithValue(ctx, ContextKeyUser, *u)
}
