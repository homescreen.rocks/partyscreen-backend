package entity

import (
	"time"

	"github.com/google/uuid"
)

const (
	MessageSourceTypeFacebook  MessageSourceType = "facebook"
	MessageSourceTypeTwitter                     = "twitter"
	MessageSourceTypeInstagram                   = "instagram"
	MessageSourceTypeLocal                       = "local"
)

type MessageSourceType string

type Message struct {
	ID      uuid.UUID `json:"id"`
	Name    string    `json:"name"`
	Date    time.Time `json:"date" storm:"index"`
	Message string    `json:"message"`
	UserID  uuid.UUID `json:"userId"`

	SourceType MessageSourceType `json:"sourceType"`
	SourceRef  string            `json:"sourceRef,omitempty"`
	SourceURL  string            `json:"sourceUrl,omitempty"`
	SourceDate time.Time         `json:"sourceDate,omitempty"`
}
