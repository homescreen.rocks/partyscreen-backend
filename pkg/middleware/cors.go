package middleware

import (
	"net/http"

	rsCors "github.com/rs/cors"
)

type Cors Interface

type CorsBuilder struct {
	handler *rsCors.Cors
}

func NewCorsBuilder() *CorsBuilder {
	return new(CorsBuilder)
}

func (b *CorsBuilder) WithAllowAll(allow bool) *CorsBuilder {
	if allow {
		b.handler = rsCors.AllowAll()
	} else {
		b.handler = rsCors.Default()
	}

	return b
}

func (b *CorsBuilder) Build() Cors {
	return &cors{
		handler: b.handler,
	}
}

type cors struct {
	handler *rsCors.Cors
}

func (mw *cors) WrapHTTP(next http.Handler, w http.ResponseWriter, req *http.Request) {
	mw.handler.Handler(next).ServeHTTP(w, req)
}
