package middleware

import (
	"net/http"
)

type Interface interface {
	WrapHTTP(http.Handler, http.ResponseWriter, *http.Request)
}

// chainLink wraps around a Interface, so it satisfies the http.Handler
// interface.
type chainLink struct {
	middleware Interface
	next       http.Handler
}

func (l *chainLink) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	l.middleware.WrapHTTP(l.next, w, r)
}

type ChainBuilder struct {
	middlewares []Interface
}

func NewChainBuilder() *ChainBuilder {
	return &ChainBuilder{
		middlewares: []Interface{},
	}
}

func (b *ChainBuilder) WithMiddleware(mw Interface) *ChainBuilder {
	// Slice needs to be reverse, since the middlewares get wrapped from the
	// inside (actual HTTP handler) to the outside (first middleware that gets
	// a request.
	b.middlewares = append([]Interface{mw}, b.middlewares...)
	return b
}

func (b *ChainBuilder) Build(final http.Handler) http.Handler {
	handler := final
	for _, mw := range b.middlewares {
		handler = &chainLink{
			middleware: mw,
			next:       handler,
		}
	}

	return handler
}
