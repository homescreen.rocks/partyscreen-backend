package controller

import (
	"io"
	"net/http"
	"strings"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/repository"
)

type Image http.Handler

type image struct {
	l               logrus.FieldLogger
	imageRepository repository.Image
}

func NewImage(l *logrus.Logger, ir repository.Image) Image {
	return &image{
		l:               l.WithField("Component", "ImageController"),
		imageRepository: ir,
	}
}

func (c *image) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	id, err := uuid.Parse(strings.Trim(r.URL.Path, "/"))
	if err != nil {
		c.l.WithFields(logrus.Fields{
			"error": err,
			"uuid":  r.URL.Path,
		}).Debugf("unable to decode image UUID")
		w.WriteHeader(http.StatusNotFound)
		return
	}

	image, err := c.imageRepository.Get(id)
	if err != nil {
		c.l.WithFields(logrus.Fields{
			"error": err,
			"uuid":  id,
		}).Debugf("unable get image")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer image.Close()

	_, err = io.Copy(w, image.Content)
	if err != nil {
		c.l.WithFields(logrus.Fields{
			"error": err,
			"uuid":  id,
		}).Debugf("unable send image")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
