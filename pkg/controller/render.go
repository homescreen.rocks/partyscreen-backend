package controller

import (
	"encoding/json"
	"net/http"

	log "github.com/sirupsen/logrus"
)

const (
	MimeJSON        = "application/json"
	MimeOctetStream = "application/octet-stream"
)

func renderJSON(w http.ResponseWriter, r *http.Request, d interface{}) {
	w.Header().Set("Content-Type", MimeJSON)

	err := json.NewEncoder(w).Encode(d)
	if err != nil {
		log.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
