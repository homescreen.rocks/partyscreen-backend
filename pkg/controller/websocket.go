package controller

import (
	"net/http"

	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/service"
)

type Socket http.Handler

type socket struct {
	l                logrus.FieldLogger
	upgrader         websocket.Upgrader
	broadcastService service.Broadcast
}

func NewSocket(l *logrus.Logger,
	broadcastService service.Broadcast) Socket {
	return &socket{
		l:                l.WithField("Component", "SocketController"),
		broadcastService: broadcastService,

		upgrader: websocket.Upgrader{
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
		},
	}
}

func (c *socket) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	conn, err := c.upgrader.Upgrade(w, r, nil)
	if err != nil {
		c.l.Warnf("failed to upgrade: %s", err)
		return
	}
	defer conn.Close()

	events := c.broadcastService.Register()
	defer c.broadcastService.Unregister(events)

	for e := range events.C() {
		err := conn.WriteJSON(e)
		if err != nil {
			c.l.Warnf("failed to send event: %s", err)
			return
		}
	}
}
