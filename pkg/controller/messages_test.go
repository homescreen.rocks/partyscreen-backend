package controller_test

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/sirupsen/logrus"

	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/controller"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/entity"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/integration"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/repository"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/testhelper"
)

func TestMessagesLifecycle(t *testing.T) {
	logrus.SetLevel(logrus.DebugLevel)

	dir, deferer := testhelper.TempDir(t, "bolt")
	defer deferer()

	db, err := integration.NewStormDBBuilder().WithDataDirectory(dir).Build()
	testhelper.Must(t, err)
	defer db.Close()

	messageRepo, err := repository.NewMessage(
		logrus.StandardLogger(),
		db)
	testhelper.Must(t, err)

	imageRepo, err := repository.NewImageBuilder().
		WithDataDirectory(dir).
		Build(logrus.StandardLogger())
	testhelper.Must(t, err)

	ctrl := controller.NewMessage(
		logrus.StandardLogger(),
		messageRepo, imageRepo)

	var (
		redPNG = "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mOUZGD4DwABbQEafpSlYQAAAABJRU5ErkJggg=="
	)

	user, err := entity.NewUser()
	testhelper.Must(t, err)

	var handler http.HandlerFunc
	handler = func(w http.ResponseWriter, r *http.Request) {
		r = r.WithContext(user.NewContext(r.Context()))
		ctrl.ServeHTTP(w, r)
	}

	ts := httptest.NewServer(handler)
	defer ts.Close()

	getMessages := func(t *testing.T) controller.Messages {
		res, err := http.Get(ts.URL)
		testhelper.Must(t, err)

		if res.StatusCode != http.StatusOK {
			t.Fatalf("Wrong status code. Have: %d. Want: %d.",
				res.StatusCode, http.StatusOK)
		}

		messages := new(controller.Messages)
		err = json.NewDecoder(res.Body).Decode(messages)
		testhelper.Must(t, err)
		res.Body.Close()

		return *messages
	}

	putMessages := func(t *testing.T, msg controller.MessageRequest) {
		buf := new(bytes.Buffer)
		err := json.NewEncoder(buf).Encode(msg)
		testhelper.Must(t, err)

		res, err := http.Post(ts.URL, "application/json", buf)
		testhelper.Must(t, err)
		defer res.Body.Close()

		if res.StatusCode != http.StatusCreated {
			t.Fatalf("Wrong status code. Have: %d. Want: %d.",
				res.StatusCode, http.StatusCreated)
		}
	}

	t.Run("InitialList", func(t *testing.T) {
		messages := getMessages(t)

		if len(messages.Items) != 0 {
			t.Fatalf("Wrong message count. Have: %d. Want: 0.",
				len(messages.Items))
		}
	})

	t.Run("PutMessage1", func(t *testing.T) {
		putMessages(t, controller.MessageRequest{
			Name:    "bimbaz",
			Message: "trolololol",
			Image: &controller.ImageRequest{
				Base64: "data:image/png;base64," + redPNG,
			},
		})
	})

	t.Run("ListMessage1", func(t *testing.T) {
		messages := getMessages(t)

		if len(messages.Items) != 1 {
			t.Fatalf("Wrong message count. Have: %d. Want: 1.",
				len(messages.Items))
		}

		message := messages.Items[0]

		haveImageURL := message.SourceURL
		wantImageURL := fmt.Sprintf("/api/v1/images/%s", message.ID)

		if haveImageURL != wantImageURL {
			t.Fatalf("Wrong image URL. Have: %s. Want: %s.",
				haveImageURL, wantImageURL)
		}

		image, err := imageRepo.Get(message.ID)
		testhelper.Must(t, err)

		haveBuf := new(bytes.Buffer)
		haveEncoder := base64.NewEncoder(base64.StdEncoding, haveBuf)
		_, err = io.Copy(haveEncoder, image.Content)
		haveEncoder.Close()
		testhelper.Must(t, err)

		haveBase64 := haveBuf.String()
		wantBase64 := redPNG

		if haveBase64 != wantBase64 {
			t.Fatalf("Images do not match.\n\tHave: %s.\n\tWant: %s.", haveBase64, wantBase64)
		}
	})
}
