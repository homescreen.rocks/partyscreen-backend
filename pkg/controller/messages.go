package controller

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"

	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/entity"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/repository"
)

const MessagesMaxPageSize = 1000

type MessageRequest struct {
	Name    string        `json:"name"`
	Message string        `json:"message"`
	Image   *ImageRequest `json:"image"`
}

type ImageRequest struct {
	Base64 string `json:"base64"`
}

type Messages struct {
	Items []entity.Message `json:"items"`
}

type Message http.Handler

type message struct {
	l                 logrus.FieldLogger
	messageRepository repository.Message
	imageRepository   repository.Image
}

func NewMessage(l *logrus.Logger,
	messageRepository repository.Message,
	imageRepository repository.Image) Message {
	return &message{
		l:                 l.WithField("Component", "MessageController"),
		messageRepository: messageRepository,
		imageRepository:   imageRepository,
	}
}

func (c *message) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error

	c.l.WithField("URL", r.URL.String()).
		Debugf("Incomming request")
	if r.URL.String() != "/" {
		c.l.WithField("URL", r.URL.String()).
			Warnf("Not Found")
		w.WriteHeader(http.StatusNotFound)
		return
	}

	switch r.Method {
	case http.MethodGet:
		user, ok := entity.UserFromContext(r.Context())
		if !ok {
			c.l.Errorf("context does not have a user")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		views := r.URL.Query()["view"]
		view := ""
		if len(views) > 1 {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if len(views) == 1 {
			view = views[0]
		}

		limits := r.URL.Query()["limit"]
		limit := MessagesMaxPageSize
		if len(limits) > 1 {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if len(limits) == 1 {
			limit, err = strconv.Atoi(limits[0])
			if err != nil {
				c.l.Errorf("faild to convert to int %s", limits[0])
				w.WriteHeader(http.StatusBadRequest)
				return
			}
		}

		if limit > MessagesMaxPageSize {
			limit = MessagesMaxPageSize
		}

		resp := Messages{}
		resp.Items = []entity.Message{}

		messages, err := c.messageRepository.List(limit)
		if err != nil {
			c.l.Errorf("request failed: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		for _, message := range messages {
			if view == "own" && message.UserID.String() != user.ID.String() {
				continue
			}

			resp.Items = append(resp.Items, message)
		}

		renderJSON(w, r, resp)

	case http.MethodPost:
		req := new(MessageRequest)
		err := json.NewDecoder(r.Body).Decode(req)
		if err != nil {
			c.l.Errorf("request failed: %v", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		user, ok := entity.UserFromContext(r.Context())
		if !ok {
			c.l.Errorf("context does not have a user")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		id := uuid.New()

		message := entity.Message{
			ID:      id,
			Name:    req.Name,
			Date:    time.Now(),
			Message: req.Message,
			UserID:  user.ID,

			SourceType: entity.MessageSourceTypeLocal,
		}

		if req.Image != nil {
			message.SourceURL = fmt.Sprintf("/api/v1/images/%s", id.String()) // TODO: Should get generated on GET and not on POST.

			image, err := entity.NewImageFromDataURL(id, req.Image.Base64)
			if err != nil {
				c.l.Errorf("request failed: %v", err)
				w.WriteHeader(http.StatusBadRequest)
				return
			}

			err = c.imageRepository.Save(*image)
			if err != nil {
				c.l.Errorf("request failed: %v", err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
		}

		err = c.messageRepository.Save(message)
		if err != nil {
			c.l.Errorf("request failed: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusCreated)

	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}
