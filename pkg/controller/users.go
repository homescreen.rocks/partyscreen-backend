package controller

import (
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/entity"
)

type User http.Handler

type user struct {
	l logrus.FieldLogger
}

func NewUser(l *logrus.Logger) User {
	return &user{
		l: l.WithField("Component", "UserController"),
	}
}

func (c *user) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	if r.URL.Path != "" {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	user, ok := entity.UserFromContext(r.Context())
	if !ok {
		logrus.Errorf("context does not have a user")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	user.Token = ""

	renderJSON(w, r, user)
}
