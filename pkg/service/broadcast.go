package service

import (
	"github.com/sirupsen/logrus"
)

type BroadcastEventType string

type BroadcastEvent struct {
	Type    BroadcastEventType `json:"event"`
	Payload interface{}        `json:"payload"`
}

type BroadcastReceiver struct {
	c chan BroadcastEvent
}

func (r *BroadcastReceiver) C() <-chan BroadcastEvent {
	return r.c
}

type Broadcast interface {
	Register() *BroadcastReceiver
	Unregister(*BroadcastReceiver)
	Emit(BroadcastEvent)
}

type broadcast struct {
	l logrus.FieldLogger

	chans map[chan<- BroadcastEvent]struct{}
}

func NewBroadcast(l *logrus.Logger) Broadcast {
	return &broadcast{
		l:     l.WithField("Component", "BroadcastService"),
		chans: map[chan<- BroadcastEvent]struct{}{},
	}
}

func (s *broadcast) Register() *BroadcastReceiver {
	r := &BroadcastReceiver{
		c: make(chan BroadcastEvent, 10),
	}

	s.chans[r.c] = struct{}{}
	s.l.Debugf("Added channel. %d registered.", len(s.chans))
	return r
}

func (s *broadcast) Unregister(r *BroadcastReceiver) {
	close(r.c)
	delete(s.chans, r.c)
	s.l.Debugf("Removed channel. %d left.", len(s.chans))
}

func (s *broadcast) Emit(e BroadcastEvent) {
	for c, _ := range s.chans {
		select {
		case c <- e:
			// sent message successfully
		default:
			s.l.Errorf("failed to send event, because channel is blocked")
		}
	}
}
