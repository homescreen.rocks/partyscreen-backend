package service

import (
	"time"

	"github.com/ahmdrz/goinsta"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/entity"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/repository"
)

type InstagramOptions struct {
	Username string
	Password string
}

type Instagram interface {
	Run()
}

type insta struct {
	l      logrus.FieldLogger
	client *goinsta.Instagram

	messageRepository repository.Message

	refCache map[string]struct{}
}

func NewInstagram(l *logrus.Logger, opts *InstagramOptions,
	messageRepository repository.Message) Instagram {
	sl := l.WithField("Component", "InstagramService")

	client := goinsta.New(opts.Username, opts.Password)
	err := client.Login()
	if err != nil {
		sl.Errorf("Failed to log into instagram: %v", err)
		return nil
	}

	return &insta{
		l: sl,

		messageRepository: messageRepository,

		client:   client,
		refCache: map[string]struct{}{},
	}
}

func (s *insta) Run() {
	ticker := time.Tick(30 * time.Second)
	for {
		err := s.fetch()
		if err != nil {
			s.l.Error(err)
		}
		<-ticker
	}
}

func (s *insta) fetch() error {
	res, err := s.client.Search.FeedTags("bachjetzt")
	if err != nil {
		return err
	}

	s.l.Debugf("fetched %d images", len(res.Images))

	for _, i := range res.Images {
		_, cached := s.refCache[i.ID]
		if cached {
			continue
		}

		if i.Images.GetBest() == "" {
			s.l.Debugf("image does not have an URL; skipping")
			s.refCache[i.ID] = struct{}{}
			continue
		}

		exists, err := s.messageRepository.RefExist(entity.MessageSourceTypeInstagram, i.ID)
		if err != nil {
			s.l.Errorf("failed to get message by ref: %v", err)
			continue
		}

		if exists {
			s.l.Debugf("ref already in database; skipping")
			s.refCache[i.ID] = struct{}{}
			continue
		}

		msg := entity.Message{
			ID:      uuid.New(),
			Name:    i.User.Username,
			Date:    time.Now(),
			Message: i.Caption.Text,

			SourceType: entity.MessageSourceTypeInstagram,
			SourceRef:  i.ID,
			SourceURL:  i.Images.GetBest(),
			SourceDate: time.Unix(int64(i.TakenAt), 0),
		}

		s.l.Debugf("saving new image from %s", msg.Name)
		err = s.messageRepository.Save(msg)
		if err != nil {
			s.l.Error(err)
			continue
		}

		s.refCache[i.ID] = struct{}{}
	}

	return nil
}
