package util_test

import (
	"testing"

	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/testhelper"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/util"
)

func TestGenerateSecret(t *testing.T) {
	s1, err := util.GenerateSecret(100)
	testhelper.Must(t, err)

	s2, err := util.GenerateSecret(100)
	testhelper.Must(t, err)

	if len(s1) != len(s2) {
		t.Fatalf("Secrets should have the same length. Got %d and %d.", len(s1), len(s2))
	}

	if len(s1) == 0 {
		t.Fatalf("Secrets be greater than 0.")
	}

	if s1 == s2 {
		t.Fatalf("Secrets not be identical.")
	}

}
