package util

import (
	"crypto/rand"
	"encoding/base64"
)

func GenerateSecret(len int) (string, error) {
	random_data := make([]byte, len)

	_, err := rand.Read(random_data)
	if err != nil {
		return "", err
	}

	return base64.RawStdEncoding.EncodeToString(random_data), nil
}
