package repository_test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/entity"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/integration"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/repository"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/testhelper"
)

func TestMessagesLifecycle(t *testing.T) {
	dir, deferer := testhelper.TempDir(t, "messages")
	defer deferer()

	db, err := integration.NewStormDBBuilder().
		WithDataDirectory(dir).
		Build()
	testhelper.Must(t, err)
	defer db.Close()

	repo, err := repository.NewMessage(
		logrus.StandardLogger(),
		db,
	)
	testhelper.Must(t, err)

	msg1 := entity.Message{
		ID:         uuid.New(),
		Name:       "test",
		Date:       time.Now(),
		Message:    "foobar",
		SourceType: entity.MessageSourceTypeLocal,
		SourceURL:  "",
	}

	msg2 := entity.Message{
		ID:         uuid.New(),
		Name:       "test",
		Date:       time.Now(),
		Message:    "foobar",
		SourceType: entity.MessageSourceTypeLocal,
		SourceURL:  "",
	}

	t.Run("InitialList", func(t *testing.T) {
		all, err := repo.List(100)
		testhelper.Must(t, err)

		if len(all) != 0 {
			t.Fatalf("Wrong message count. Have: %d. Want 0.", len(all))
		}
	})

	t.Run("SaveMessage1", func(t *testing.T) {
		testhelper.Must(t, repo.Save(msg1))
	})

	t.Run("ListMessage1", func(t *testing.T) {
		all, err := repo.List(100)
		testhelper.Must(t, err)

		if len(all) != 1 {
			t.Fatalf("Wrong message count. Have: %d. Want 0.", len(all))
		}

		have := all[0]
		if have.ID.ID() != msg1.ID.ID() {
			t.Fatalf("Wrong message ID. Have: %s. Want %s.",
				msg1.ID.String(), have.ID.String())
		}
	})

	t.Run("SaveMessage1Again", func(t *testing.T) {
		testhelper.Must(t, repo.Save(msg1))
	})

	t.Run("ListMessage1Again", func(t *testing.T) {
		all, err := repo.List(100)
		testhelper.Must(t, err)

		if len(all) != 1 {
			t.Fatalf("Wrong message count. Have: %d. Want 0.", len(all))
		}
	})

	t.Run("SaveMessage2", func(t *testing.T) {
		testhelper.Must(t, repo.Save(msg2))
	})

	t.Run("List2Messages", func(t *testing.T) {
		all, err := repo.List(100)
		testhelper.Must(t, err)

		if len(all) != 2 {
			t.Fatalf("Wrong message count. Have: %d. Want 0.", len(all))
		}
	})
}
