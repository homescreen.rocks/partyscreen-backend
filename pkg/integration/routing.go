package integration

import (
	"net/http"
	"strings"

	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/controller"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/middleware"
	"go.uber.org/fx"
)

type RouterBuilder struct {
	uiDirectory string
}

func NewRouterBuilder() *RouterBuilder {
	return new(RouterBuilder)
}

func (b *RouterBuilder) WithUIDirectory(dir string) *RouterBuilder {
	b.uiDirectory = dir
	return b
}

type RouterParams struct {
	fx.In

	MessageController controller.Message
	ImageController   controller.Image
	StreamController  controller.Stream
	UserController    controller.User
	SocketController  controller.Socket

	//AuthMiddleware middleware.Auth
	CorsMiddleware middleware.Cors
}

func (b *RouterBuilder) Build(p RouterParams) http.Handler {
	mux := http.NewServeMux()

	handle := func(pattern string, handlerFunc http.Handler) {
		handler := http.Handler(handlerFunc)
		stripped := http.StripPrefix(strings.TrimRight(pattern, "/"), handler)
		mux.Handle(pattern, stripped)
	}

	if b.uiDirectory != "" {
		mux.Handle("/", http.FileServer(http.Dir(b.uiDirectory)))
	}

	handle("/api/v1/messages/", p.MessageController)
	handle("/api/v1/images/", p.ImageController)
	handle("/api/v1/stream/", p.StreamController)
	handle("/api/v1/users/", p.UserController)
	handle("/api/v1/socket/", p.SocketController)

	return middleware.NewChainBuilder().
		WithMiddleware(p.CorsMiddleware).
		//WithMiddleware(p.AuthMiddleware).
		Build(mux)
}
