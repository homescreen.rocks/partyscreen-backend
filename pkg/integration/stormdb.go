package integration

import (
	"os"
	"path"

	"github.com/asdine/storm"
)

type StormDBBuilder struct {
	dataDir  string
	boltFile string
}

func NewStormDBBuilder() *StormDBBuilder {
	return &StormDBBuilder{
		boltFile: "partyscreen.db",
	}
}

func (b *StormDBBuilder) WithDataDirectory(dir string) *StormDBBuilder {
	b.dataDir = dir
	return b
}

func (b *StormDBBuilder) Build() (*storm.DB, error) {
	err := os.MkdirAll(b.dataDir, 0755)
	if err != nil {
		return nil, err
	}

	db, err := storm.Open(path.Join(b.dataDir, b.boltFile))
	if err != nil {
		return nil, err
	}

	return db, nil
}
