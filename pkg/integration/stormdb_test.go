package integration_test

import (
	"testing"

	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/integration"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/testhelper"
)

func TestStormDBCreation(t *testing.T) {
	dir, deferer := testhelper.TempDir(t, "bolt")
	defer deferer()

	db, err := integration.NewStormDBBuilder().
		WithDataDirectory(dir).
		Build()
	testhelper.Must(t, err)

	err = db.Close()
	testhelper.Must(t, err)
}
