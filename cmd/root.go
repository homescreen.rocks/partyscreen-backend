package cmd

import (
	"net/http"
	"time"

	"github.com/rebuy-de/rebuy-go-sdk/cmdutil"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"go.uber.org/fx"

	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/controller"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/integration"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/middleware"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/repository"
	"gitlab.com/homescreen.rocks/partyscreen-backend/pkg/service"
)

func NewRootCommand() *cobra.Command {
	cmd := cmdutil.NewRootCommand(new(App))
	cmd.Short = "partyscreen-backend is a backend for a screen on a party"
	return cmd
}

type App struct {
	Params struct {
		ListenAddress string
		UIDirectory   string
		DataDir       string
		CORSAllowAll  bool
	}
	InstagramOptions service.InstagramOptions
}

func (app *App) Bind(cmd *cobra.Command) {
	cmd.PersistentFlags().StringVarP(
		&app.Params.ListenAddress, "listen-address", "l", ":9999",
		"Listen address of the HTTP server (format: [ip]:port)")
	cmd.PersistentFlags().StringVarP(
		&app.Params.UIDirectory, "ui-directory", "d", "",
		"Path to directory that contains the frontend files")
	cmd.PersistentFlags().StringVar(
		&app.Params.DataDir, "data-directory", "./data",
		"Path to directory that contains the database file and pictures.")
	cmd.PersistentFlags().BoolVar(
		&app.Params.CORSAllowAll, "cors-allow-all", false,
		"Allows all origins with all standard methods with any header and credentials.")
	cmd.PersistentFlags().StringVar(
		&app.InstagramOptions.Username, "instagram-username", "",
		"Username for Instagram.")
	cmd.PersistentFlags().StringVar(
		&app.InstagramOptions.Password, "instagram-password", "",
		"Password for Instagram.")
}

type fxLogger struct{}

func (l *fxLogger) Printf(m string, a ...interface{}) {
	log.WithField("Component", "fx").Debugf(m, a...)
}

func (app *App) Run(cmd *cobra.Command, args []string) {
	fx.New(
		fx.Provide(func() (*service.Options, *service.InstagramOptions) {
			return &service.Options{
				Interval:    20 * time.Second,
				MaxBacklog:  50,
				HistorySize: 12,
			}, &app.InstagramOptions
		}),
		fx.Logger(new(fxLogger)),

		fx.Provide(
			log.StandardLogger,
			http.NewServeMux,

			integration.NewStormDBBuilder().
				WithDataDirectory(app.Params.DataDir).
				Build,
			integration.NewRouterBuilder().
				WithUIDirectory(app.Params.UIDirectory).
				Build,

			repository.NewImageBuilder().
				WithDataDirectory(app.Params.DataDir).
				Build,
			repository.NewMessage,
			repository.NewUser,

			controller.NewMessage,
			controller.NewImage,
			controller.NewStream,
			controller.NewUser,
			controller.NewSocket,

			//middleware.NewAuth,
			middleware.NewCorsBuilder().
				WithAllowAll(app.Params.CORSAllowAll).
				Build,

			service.NewStream,
			service.NewBroadcast,
			service.NewInstagram,
		),

		fx.Invoke(func(stream service.Stream, instagram service.Instagram) {
			go stream.Run()
			if instagram != nil {
				go instagram.Run()
			}
		}),

		fx.Invoke(func(h http.Handler) {
			err := http.ListenAndServe(app.Params.ListenAddress, h)
			if err != nil {
				log.Errorf("failed to start server: %v", err)
				cmdutil.Exit(1)
			}
		}),
	).Run()

}
