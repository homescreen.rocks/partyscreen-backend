package main

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/homescreen.rocks/partyscreen-backend/cmd"
	"github.com/rebuy-de/rebuy-go-sdk/cmdutil"
)

func main() {
	defer cmdutil.HandleExit()
	if err := cmd.NewRootCommand().Execute(); err != nil {
		log.Fatal(err)
	}
}
